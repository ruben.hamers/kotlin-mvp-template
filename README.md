# kotlin-mvp-template

<i>Template for kotlin android MVP + Clean Architecture projects, without dependencies to popular external libraries like RXJava or Dagger2.</i>

# Introduction
I decided to create a Kotlin + Android app as a hobby project since I am not familiar with the Kotlin language and Android app development in general.
I also wanted to implement the UI with a Model View Presenter (MVP) architecture and a domain that adheres to Clean Architecture principles.
There are many well documented examples to be found on Kotlin+Android+mvp but they all heavily depend upon RXJava, Dagger2, and several kinds of mocking / testing frameworks.
In some examples, these dependencies reach deeply into the domain layer, which I think breaks a major assumption given by Clean Architecture where outer layers depend on inner layers, not the other way around.
So that is why I decided to try and make my own solution without any of the external libraries.

# Setup

Currently there is not much setup to be done, just make sure you setup a config for an emulator and for unit testing.

# Continuous Integration
If you use Gitlab as VCS the continuous integration (CI) is already setup and will run out of the box. No need for setting up environment variables or other configuration.
The CI consists of two stages, a build stage and a test stage.
The build stage will make sure that the app compiles (if it succeeds, you can download it from the pipeline stage artifacts), and it will lint the repo for correct code conventions.
The test stage will run all tests, currently it does <b>not</b> run instrumentation tests.

# Layers

At this moment, the app consists of 4 main packages:


|Packages|Description |
|----|:----:|
|Data| This is where all the implementation of the Gateways i.e. Repositories is done, they all depend on interfaces defined in the domain layer|
|Domain| This is the core of the application, it defines all entities (business objects), repositories, and Interactors (Usecases) that 'control the dance between the entities'|
|Main| Main is used to inject all dependencies to the application|
|UI| This is where all UI logic lives, all models, views and presenters|

# Managing State
I found it pretty difficult to find a nice way to manage (central) state in an Android Application without the use of Singleton.
(I do not want to use a Singleton since it will make testing more difficult plus they do not really support abstraction)
Currently the best solution I came up with was to extend the Application class and manage the state there, but there is probably a better way to do this.

# ToDo
1. Fix Instrumentation testing
2. Find a better way to manage state
3. Write a custom lint.xml to extend the lint process

