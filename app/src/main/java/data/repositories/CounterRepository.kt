package data.repositories

import domain.entities.counter.Counter
import domain.entities.counter.ICounterRepository
import domain.entities.repository.BaseRepository

class CounterRepository : BaseRepository<Counter>(), ICounterRepository {

}