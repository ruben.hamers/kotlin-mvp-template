package com.example.mvp_example.ui.counter

import com.example.mvp_example.ui.contract.BaseContract

class CounterModel(private var counter: Int) : BaseContract.Model {

    fun getCounter(): String {
        return counter.toString()
    }

    fun setCounter(c: Int?) {
        if (c != null) {
            counter = c
        }
    }
}