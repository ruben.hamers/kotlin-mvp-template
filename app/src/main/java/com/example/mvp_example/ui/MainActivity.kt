package com.example.mvp_example.ui

import Main.MainApplication
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.mvp_example.R
import com.example.mvp_example.ui.contract.BaseContract.View.Companion.parseParam
import com.example.mvp_example.ui.counter.CounterContract
import com.example.mvp_example.ui.counter.CounterPresenter
import domain.entities.counter.ICounterRepository
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), CounterContract.View {

    private var presenter: CounterPresenter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val mainApp = application as MainApplication
        val counterRepository = mainApp.repositoryFactory.getRepository<ICounterRepository>()
        presenter = CounterPresenter(this, counterRepository)
    }

    override fun setCounter(counter: String) {
        counterTextView.text = counter
    }

    override fun initView(params: List<Any>) {
        setCounter(parseParam<String>(params))
        clickButton.setOnClickListener { presenter?.onIncrementClick() }
    }

    override fun updateViewData(params: List<Any>) {
        setCounter(parseParam<String>(params))
    }

}
