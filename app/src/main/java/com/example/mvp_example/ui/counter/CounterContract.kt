package com.example.mvp_example.ui.counter

import com.example.mvp_example.ui.contract.BaseContract

class CounterContract {

        interface View: BaseContract.View {
            fun setCounter(counter: String)
        }

        interface Presenter: BaseContract.Presenter<View> {
            fun onIncrementClick()
        }

}