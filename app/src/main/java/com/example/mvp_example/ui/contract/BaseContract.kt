package com.example.mvp_example.ui.contract

class BaseContract {

    interface Presenter<in T> {
            }

    interface View {
        fun initView(params: List<Any>)
        fun updateViewData(params: List<Any>)

        companion object{
            inline fun <reified T> parseParam(params: List<Any>) : T = params.find { p -> p is T } as T
        }
    }

    interface Model {}
}

