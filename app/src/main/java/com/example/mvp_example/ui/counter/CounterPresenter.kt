package com.example.mvp_example.ui.counter

import domain.entities.counter.ICounterRepository
import domain.usecases.counter.CreateCounter
import domain.usecases.counter.IncrementCounter

public class CounterPresenter(
    private var view: CounterContract.View,
    private val counterRepository: ICounterRepository
) : CounterContract.Presenter {

    private var model: CounterModel = CounterModel(0);
    private lateinit var counterId: String

    init {
        view.initView(listOf("0"))
        CreateCounter(counterRepository).execute { id -> counterId = id }
    }

    override fun onIncrementClick() {
        IncrementCounter(counterId, counterRepository).execute { counter -> updateCounter(counter) }
    }

    private fun updateCounter(counter: Int?) {
        model.setCounter(counter)
        view.setCounter(model.getCounter())
    }
}