package com.example.mvp_example.ui.factory

import data.repositories.CounterRepository
import domain.entities.repository.RepositoryContract.Repository

class RepositoryFactory{

    val repositories : List<Repository>

    init {
        println("RepositoryFactory invoked.")
        repositories = listOf(CounterRepository())
    }
    inline fun < reified T: Repository> getRepository() : T  {
        return repositories.find { repository -> repository is T } as T
    }
}