package domain.usecases.contracts

open class Usecase(private var callback: () -> Unit) {

    public open fun execute(callback: () -> Unit) {
        this.callback = callback;
    }

    protected fun invokeCallback() {
        this.callback.invoke()
    }
}

open class Usecase1<T> {

    private var callback: ((T) -> Unit)? = null

    public open fun execute(callback: (t: T) -> Unit) {
        this.callback = callback;
    }

    protected fun invokeCallback(t: T) {
        this.callback?.invoke(t)
    }
}

open class Usecase2<T1, T2>() {

    private var callback: ((T1, T2) -> Unit)? = null

    public open fun execute(callback: (t1: T1, t2: T2) -> Unit) {
        this.callback = callback;
    }

    protected fun invokeCallback(t1: T1, t2: T2) {
        this.callback?.invoke(t1, t2)
    }
}

open class Usecase3<T1, T2, T3>() {

    private var callback: ((T1, T2, T3) -> Unit)? = null

    public open fun execute(callback: (t1: T1, t2: T2, t3: T3) -> Unit) {
        this.callback = callback;
    }

    protected fun invokeCallback(t1: T1, t2: T2, t3: T3) {
        this.callback?.invoke(t1, t2, t3)
    }
}

open class Usecase4<T1, T2, T3, T4>() {

    private var callback: ((T1, T2, T3, T4) -> Unit)? = null

    public open fun execute(callback: (t1: T1, t2: T2, t3: T3, t4: T4) -> Unit) {
        this.callback = callback;
    }

    protected fun invokeCallback(t1: T1, t2: T2, t3: T3, t4: T4) {
        this.callback?.invoke(t1, t2, t3, t4)
    }
}