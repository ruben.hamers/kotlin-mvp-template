package domain.usecases.counter

import domain.entities.counter.Counter
import domain.entities.counter.ICounterRepository
import domain.usecases.contracts.Usecase1

class CreateCounter(private val counterRepository: ICounterRepository) : Usecase1<String>() {

    public override fun execute(callback: (T1: String) -> Unit) {
        super.execute(callback)
        val counter = Counter(0)
        counterRepository.add(counter)
        invokeCallback(counter.id)
    }
}