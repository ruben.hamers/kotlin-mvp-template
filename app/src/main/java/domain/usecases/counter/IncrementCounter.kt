package domain.usecases.counter

import domain.entities.counter.ICounterRepository
import domain.usecases.contracts.Usecase1

class IncrementCounter(val counterId: String, private val counterRepository: ICounterRepository) : Usecase1<Int?>() {

    public override fun execute(callback: (t: Int?) -> Unit) {
        super.execute(callback)
        val counter = counterRepository.get(counterId)
        counter?.increment()
        counterRepository.update(counter)
        invokeCallback(counter?.getAmount())
    }
}