package domain.entities.counter

import domain.entities.repository.RepositoryContract

interface ICounterRepository : RepositoryContract.IRepository<Counter> {
}