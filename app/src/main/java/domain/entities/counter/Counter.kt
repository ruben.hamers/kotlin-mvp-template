package domain.entities.counter

import domain.entities.BaseEntity

class Counter(private var amount: Int) : BaseEntity() {

    fun increment(): Int {
        return ++amount
    }

    fun getAmount(): Int {
        return amount
    }

}