package domain.entities

interface IEntity {
    val id: String
}