package domain.entities.repository

import domain.entities.IEntity

class RepositoryContract {

    interface Repository {}

    interface IRepository<T : IEntity> :
        Repository {
        fun add(t: T)
        fun get(id: String) : T?
        fun update(t: T?)
        fun delete(t: T?)
        fun delete(id: String)
    }
}