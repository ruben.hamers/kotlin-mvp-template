package domain.entities.repository

import domain.entities.IEntity

open abstract class BaseRepository<T: IEntity> :RepositoryContract.IRepository<T> {
    private val values: MutableMap<String, T> = mutableMapOf()

    override fun add(t: T) {
        values[t.id] = t;
    }

    override fun get(id: String): T? {
        return if (values.containsKey(id))
            values[id]
        else
            null
    }

    override fun update(t: T?) {
        if (t != null)
            values[t.id] = t
    }

    override fun delete(t: T?) {
        if (t != null)
            delete(t.id)
    }

    override fun delete(id: String) {
        if (values.containsKey(id))
            values.remove(id)
    }
}