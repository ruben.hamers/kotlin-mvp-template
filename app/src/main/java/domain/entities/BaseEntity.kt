package domain.entities

import java.util.*

open class BaseEntity() : IEntity {

    private var identifier: String;

    init {
        identifier = UUID.randomUUID().toString()
    }

    override val id: String
        get() = identifier
}