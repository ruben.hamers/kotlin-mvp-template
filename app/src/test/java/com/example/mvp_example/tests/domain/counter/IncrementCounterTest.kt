package com.example.mvp_example.tests.domain.counter

import data.repositories.CounterRepository
import domain.entities.counter.Counter
import domain.usecases.counter.IncrementCounter
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class IncrementCounterTest {

lateinit var counterRepository: CounterRepository;
    @Before
    fun setup(){
        counterRepository = CounterRepository()
    }

    @Test
    fun increment_isCorrect() {
        val counter = Counter(0)
        counterRepository.add(counter)
        val incrementer = IncrementCounter(counter.id, counterRepository)
        incrementer.execute { t -> Assert.assertEquals(1, t) }
    }

    @After
    fun teardown(){
    }
}