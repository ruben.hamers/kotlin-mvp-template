package com.example.mvp_example.tests.domain.counter

import domain.entities.counter.Counter
import org.junit.Assert
import org.junit.Test

class CounterTests {

    @Test
    fun increment_isCorrect() {
        val counter = Counter(0)

        Assert.assertEquals(1, counter.increment())
        Assert.assertEquals(1, counter.getAmount())
    }

    @Test
    fun creating_Multiple_Counters_Do_Have_Different_IDs() {
        val c = Counter(0)
        val c2 = Counter(0)

        Assert.assertNotEquals(c.id, c2.id)
    }
}