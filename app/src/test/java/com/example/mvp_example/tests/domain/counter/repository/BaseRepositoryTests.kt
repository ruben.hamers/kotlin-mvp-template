package com.example.mvp_example.tests.domain.counter.repository

import domain.entities.IEntity
import domain.entities.repository.BaseRepository
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class BaseRepositoryTests {
    class MockEntity(override val id: String) : IEntity{
        public var counter: Int = 0

        fun incrementCounter(){
            ++counter
        }
    }
    class MockRepository : BaseRepository<MockEntity>() {}

    lateinit var mockEntity : MockEntity
    lateinit var mockRepository: MockRepository

    @Before
    fun setup(){
        mockEntity = MockEntity("Foo")
        mockRepository = MockRepository()
    }

    @Test
    fun Entity_Is_Receivable_When_Added_To_The_Repository(){
        mockRepository.add(mockEntity)
        Assert.assertEquals("Foo", mockRepository.get("Foo")?.id)
    }

    @Test
    fun When_Entity_Is_Updated_In_The_Repository_You_Receive_The_Update_Entity_Upon_Get(){
        mockRepository.add(mockEntity)
        Assert.assertEquals("Foo", mockRepository.get("Foo")?.id)
        mockEntity.incrementCounter();
        Assert.assertEquals(1,mockRepository.get("Foo")?.counter)
    }
    @Test
    fun When_Entity_Is_Deleted_By_Id_The_Repository_No_Longer_Contains_it(){
        mockRepository.add(mockEntity)
        mockRepository.delete(mockEntity.id)
        Assert.assertNull(mockRepository.get("Foo"))
    }
    @Test
    fun When_Entity_Is_Deleted_By_Reference_The_Repository_No_Longer_Contains_it(){
        mockRepository.add(mockEntity)
        mockRepository.delete(mockEntity)
        Assert.assertNull(mockRepository.get("Foo"))
    }
}