package com.example.mvp_example.tests.domain.counter

import data.repositories.CounterRepository
import domain.usecases.counter.CreateCounter
import org.junit.Assert
import org.junit.Test

class CreateCounterTests {
    @Test
    fun When_CreateCounter_Is_Executed_A_New_Counter_Is_Added_To_The_Repository() {
        val counterRepository = CounterRepository()
        val creator = CreateCounter(counterRepository);
        var id : String? = null;
        creator.execute { counterId -> id = counterId}
        Assert.assertNotNull(id)
        Assert.assertNotNull(id?.let { counterRepository.get(it) })
        Assert.assertEquals(id, id?.let { counterRepository.get(it) }?.id)
    }
}